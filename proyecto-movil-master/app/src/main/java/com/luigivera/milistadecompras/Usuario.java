package com.luigivera.milistadecompras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Usuario extends AppCompatActivity implements View.OnClickListener {
    private EditText usuario, contraseña;
    private Button iniciar, registrarse;
    private ProgressDialog progressDialog;
    private ViewFlipper viewFlipper;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);


        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);
        iniciar = (Button)findViewById(R.id.BTNIniciarSesion);
        registrarse = (Button)findViewById(R.id.BTNRegistro);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        // Obtenemos una instancia de esta clase llamada getInstance().
        firebaseAuth = FirebaseAuth.getInstance();



        // preguntamos a la base de datos si tiene a ese usuario
        // si es correcto el usuario y la contraseña entonces accedemos a la interfaz de juegos en la app
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    startActivity(new Intent(Usuario.this, Lista_de_compras.class));
                    finish();
                } else {

                }
            }
        };
    }

    //  Cargamos las imágenes y le damos un tiempo límite de presentacion.
    public void fliper(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // cuando hacemos clic en el botón "iniciar sesión" este realizará las siguientes condicioens..
            case R.id.BTNIniciarSesion:
                // cuando existan campos vacios mostrará una alerta que el campo es necesario
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));
                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));
                } else {
                    if (usuario.getText().toString().isEmpty()) {
                        usuario.setError(getResources().getString(R.string.error));
                    } else if (contraseña.getText().toString().isEmpty()) {
                        contraseña.setError(getResources().getString(R.string.error));
                    } else {
                        // comprueba si la clave y correo están escrito de forma correcta
                        final String correo = usuario.getText().toString().trim();
                        String clave = contraseña.getText().toString().trim();
                        // muestra una barra de dialogo y nos dice que se "ESTÁ CONSULTANDO EN LINEA"
                        // si los datos son correctos entonces se mostrará el mensaje "ACCEDIENDO..!"
                        progressDialog.setTitle("CONSULTANDO EN LA BASE DE DATOS");
                        progressDialog.setMessage("ACCEDIENDO..!");
                        // método para mostrar la barra de dialogo.
                        progressDialog.show();
                        // entonces, comprueba con los datos que están en la base de datos firebase, si estos son iguales se accede
                        // caso contrario saldrá un mensaje "DATOS ERRONEOS"
                        firebaseAuth.signInWithEmailAndPassword(correo, clave).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Intent intent = new Intent(Usuario.this, Lista_de_compras.class);
                                startActivity(intent);
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(Usuario.this, "DATOS ERRONEOS..!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                        });
                    }
                }
                break;
            // accedems a la actividad registro.
            case R.id.BTNRegistro:
                startActivity(new Intent(Usuario.this, Registro.class));
                finish();
                break;
        }
    }
    // Este método onTouchEvent interpretará los eventos táctiles en esta actividad
    // Cuando estamos usando el teclado del celular y hacemos clic en la pantalla, entonces el teclado se esconde.
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
    // Inicializamos la bases de datos Firebase
    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }
    // La base de datos se detiene
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
        }
    }
}