package com.luigivera.milistadecompras;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class Lista_de_compras extends AppCompatActivity {
    Button botonCrear, botonPedir;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_compras);
        firebaseAuth = FirebaseAuth.getInstance();
        botonCrear = (Button) findViewById(R.id.btnCrear);
        botonPedir = (Button) findViewById(R.id.btnPedir);

        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent miIntent = new Intent(Lista_de_compras.this, Crear_mi_lista.class);
                startActivity(miIntent);

            }
        });
        botonPedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent miIntent = new Intent(Lista_de_compras.this, Pedir_ayuda.class);
                startActivity(miIntent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Cerrar_Sesion:
                firebaseAuth.signOut();
                startActivity(new Intent(Lista_de_compras.this, Usuario.class));

                break;


        }
        return true;
    }


}


