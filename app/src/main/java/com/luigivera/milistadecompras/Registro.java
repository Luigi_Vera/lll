package com.luigivera.milistadecompras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registro extends AppCompatActivity implements View.OnClickListener  {
    private EditText nombre, apellido, correo, clave, vclave;
    private Button registrar, IniciarSesion;
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference usuario;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombre =(EditText)findViewById(R.id.TXTNombres);
        apellido = (EditText)findViewById(R.id.TXTApellidos);

        correo = (EditText)findViewById(R.id.TXTCorreo);
        clave = (EditText)findViewById(R.id.TXTClave);
        vclave = (EditText)findViewById(R.id.TXTVClave);

        registrar = (Button)findViewById(R.id.BTNRegistrar);
        IniciarSesion = (Button)findViewById(R.id.BTNIniciarS);

        progressDialog = new ProgressDialog(this);

        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        usuario = firebaseDatabase.getReference("Usuarios");

        /* Asociamos el listenes a los botones*/
        registrar.setOnClickListener(this);
        IniciarSesion.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BTNRegistrar:
                /* Comprueba que el campo "nombre" esté completo*/
                if (nombre.getText().toString().isEmpty()){
                    nombre.setError(getString(R.string.error));
                    nombre.requestFocus();
                    /* Comprueba que el campo "apellido" esté completo*/
                }else if (apellido.getText().toString().isEmpty()){
                    apellido.setError(getString(R.string.error));
                    apellido.requestFocus();

                    /* Comprueba que el campo "completo" esté completo*/
                }else if (correo.getText().toString().isEmpty()){
                    correo.setError(getString(R.string.error));
                    correo.requestFocus();
                    /* Comprueba que el campo "clave" esté completo*/
                }else if (clave.getText().toString().isEmpty()){
                    clave.setError(getString(R.string.error));
                    clave.requestFocus();
                    /* Comprueba que el campo "verificar clave" esté completo*/
                }else if (vclave.getText().toString().isEmpty()) {
                    vclave.setError(getString(R.string.error));
                    vclave.requestFocus();


                }else if (validar_clave(clave.getText().toString())==true){
                    clave.setError(getString(R.string.clavevalidar));
                    clave.requestFocus();
                }else if(vclave.getText().toString().equals(clave.getText().toString())==false) {
                    vclave.setError(getString(R.string.comprobarclave) + " " + getString(R.string.password) + " " + getString(R.string.xxx));
                    vclave.requestFocus();
                }else if (validate_correo(correo.getText().toString())==false) {
                    correo.setError(getString(R.string.correovalido));
                    correo.requestFocus();


                }else{


                    progressDialog.setTitle("REGISTRO");
                    progressDialog.setMessage("REGISTRANDO USUARIO..!");
                    progressDialog.show();

                    firebaseAuth.createUserWithEmailAndPassword(correo.getText().toString(), clave.getText().toString())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    Usuario1 User = new Usuario1();
                                    User.setNombres(nombre.getText().toString());
                                    User.setApellidos(apellido.getText().toString());
                                    User.setCorreo(correo.getText().toString());
                                    User.setClave(clave.getText().toString());
                                    User.setVclave(vclave.getText().toString());


                                    usuario.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(User)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Intent intent = new Intent(Registro.this, Lista_de_compras.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });
                                }

                            });


                }
                break;
            case R.id.BTNIniciarS:
                startActivity(new Intent(Registro.this, Usuario.class));
                finish();
                break;
        }
    }

      public boolean validar_clave(String password){
        String PASSWORD_PATTERN ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
    public boolean validate_correo(String email){
        String EMAIL_ADDRESS ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern1 = Pattern.compile(EMAIL_ADDRESS);
        Matcher matcher1 = pattern1.matcher(email);
        return matcher1.matches();
    }

    @Override
    public void onBackPressed(){

    }

}


